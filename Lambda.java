import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Lambda {
    public static void main(String[] args) throws Exception  {
        //Example of a custom made lambda expression taking in two arguments.
        Button chaosButton = new Button();
        chaosButton.push((s,x) -> System.out.println(s + ": " + x));

        //Function<A,B> takes in type as argument A and returns type B.
        //Stream's map-method uses a Function as parameter.
        Function<Integer, Integer> func = x -> x + 1;
        System.out.println(func.apply(2));

        //Predicate<A> takes in type A and returns a boolean of whether the specified condition is fulfilled or not.
        //Streams's filter-method uses a Predicate as parameter.
        Predicate<Integer> pred = x -> x % 2 == 0;
        System.out.println(pred.test(2));

        //Consumer<A> takes in type A and applies it to a void function. Therefore it has no return type.
        //Streams's forEach-method uses a Consumer as parameter.
        //Since the return type is void, the stream "ends" after forEach is called.
        Consumer<Integer> c = x -> System.out.println(x);

        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 1; i <= 100; i++) {
            list.add(i);
        }

        //Example of how to use streams and their methods.
        System.out.println("Example of some random method usage for streams:");
        List e = list.stream().filter(x -> x % 2 == 0).map(x -> Math.sqrt(x)).map(x -> Math.round(x)).collect(Collectors.toList());
        System.out.println(e);

        //We can also read text files and parse them to streams like this.
        Stream<String> stream = Files.lines(Paths.get("file.txt")).filter(line -> Character.isDigit(line.charAt(0)));

        //We sum the the values on each row of the text file.
        System.out.println("Generated output of our text file:");
        stream.map(s -> s.split(" ")).map(s -> convert(s)).map(x -> sum(x)).forEach(c);

        ArrayList<Integer> l = new ArrayList<>();
        l.add(1); l.add(2); l.add(3);

        //We can calculate the product of all the elements in a stream by using the method reduce.
        //reduce(A, B) takes in a start value A and a lambda expression with two parameters B.
        //Keep in mind that reduce is not ensured to map the lambda function in a specific order.
        System.out.println("Reduce method of " + l + " (1 * 1 * 2 * 3):");
        System.out.println(l.stream().reduce(1, (x,y) -> x * y));
    }

    static int[] convert(String[] s) {
        int[] ans = new int[s.length];
        for (int i = 0; i < ans.length; i++) {
            ans[i] = Integer.parseInt(s[i]);
        }
        return ans;
    }

    static int sum(int[] a) {
        int sum = 0;
        for (int i = 0; i < a.length; i++) {
            sum += a[i];
        }
        return sum;
    }
}

class Button {
    void push(Action a) {
        a.doAction("The number of the beast", 666);
    }
}

interface Action {
    void doAction(String s, int x);
}
